//
//  LookupsManager.m
//  tafweej
//
//  Created by MOHAMMED TAQI AHMED MIR on 8/17/16.
//  Copyright © 2016 com.sejeltech. All rights reserved.
//

#import "LookupsManager.h"

#define TOTAL_SERVICES_TO_BE_SYNCED 2

@interface LookupsManager(){
    BOOL IS_INLOADING_STATE;
    int SyncedServices;

}

@end

static LookupsManager *sharedLookupsHandler = nil;


@implementation LookupsManager


@synthesize completionHandler;
@synthesize errorHandler;

+(LookupsManager*)sharedLookupsHandler
{
    @synchronized([LookupsManager class])
    {
        if (!sharedLookupsHandler)
            sharedLookupsHandler = [[self alloc] init];
        
    }
    
    return sharedLookupsHandler;
}

+(id)alloc
{
    @synchronized([LookupsManager class])
    {
        NSAssert(sharedLookupsHandler== nil, @"Attempted to allocate a second instance of a singleton.");
        sharedLookupsHandler = [super alloc];
        return sharedLookupsHandler;
    }
}

-(id)init {
    self = [super init];
    if (self != nil) {
    }
    return self;
}


-(void)startSyncing{
    SyncedServices = 0 ;
    [self syncCountries];
    [self syncCities];
//   // [self synHotels];
//   // [self synFlights];
//    [self synNationalities];
//    [self syncPorts];
//    [self syncContracts];
//    [self syncIstikbalPaths];
//    [self syncLandTransactions];
//    [self syncHouses];
//    [self syncHajMissions];
//    [self syncHajCompanies];
    

}
-(void)syncPorts{
     [self fetchLookupDataFor:[WebServicesURLs getPortsLookupURL] params:nil withSelector:@selector(savePorts:)];
}
-(void)synNationalities{
    [self fetchLookupDataFor:[WebServicesURLs getNationalitiesLookupURL] params:nil withSelector:@selector(saveNationalities:)];
}
-(void)syncContracts{
     [self fetchLookupDataFor:[WebServicesURLs getSpecialConstractsLookupURL] params:nil withSelector:@selector(saveContracts:)];
}
-(void)syncIstikbalPaths{
     [self fetchLookupDataFor:[WebServicesURLs getIstikbalPathLookupURL] params:nil withSelector:@selector(savePaths:)];
}
-(void)syncLandTransactions{
     [self fetchLookupDataFor:[WebServicesURLs getTransportCompaniesLookupURL] params:nil withSelector:@selector(saveTransportCompanies:)];
}

-(void)syncCountries{
    
   // [self fetchLookupDataFor:[WebServicesURLs getCountriesLookupURL] params:nil withSelector:@selector(saveCountries:)];

}
-(void)syncHouses{
    
    [self fetchLookupDataFor:[WebServicesURLs getHousingLookupURL] params:nil withSelector:@selector(saveHouses:)];
    
}
-(void)syncHajCompanies{
    
    [self fetchLookupDataFor:[WebServicesURLs getHajCompaniesURL] params:nil withSelector:@selector(saveHajCompanies:)];
    
}
-(void)syncHajMissions{
    
    [self fetchLookupDataFor:[WebServicesURLs getHajMissionURL] params:nil withSelector:@selector(saveHajMissions:)];
    
}
-(void)syncCities{
    [self fetchLookupDataFor:[WebServicesURLs getCitiesLookupURL] params:nil withSelector:@selector(saveCities:)];
}
-(void)synHotels{
    [self fetchLookupDataFor:[WebServicesURLs getHotelsLookupURL] params:nil withSelector:@selector(saveHotels:)];
}
-(void)synFlights{
    [self fetchLookupDataFor:[WebServicesURLs getFlightsURL] params:nil withSelector:@selector(saveFlights:)];
}

-(void)fetchLookupDataFor:(NSString *)urlString params:(NSMutableDictionary *)parameters withSelector:(SEL )selector{
    WebServicesManager *serviceManager =  [[WebServicesManager alloc]init];
    [serviceManager fetchData:urlString  withParamters:parameters withSelector:selector];
    
    [serviceManager setCompletionHandler:^(NSArray *response , SEL selector){
        if(response.count>0){
         
            if(selector == @selector(saveCities:))
               [self saveCities:response ];
            else if(selector ==@selector(saveCountries:))
                [self saveCountries:response];
            else if(selector ==@selector(saveHotels:))
                [self saveHotels:response];
            else if(selector ==@selector(saveFlights:))
                [self saveFlights:response];
            else if(selector ==@selector(savePorts:))
                [self savePorts: [response valueForKey:@"ports"]];
            else if(selector ==@selector(saveTransportCompanies:))
                [self saveTransportCompanies:[response valueForKey:@"landTransCompanies"]];
            else if(selector ==@selector(savePaths:))
                [self savePaths:[response valueForKey:@"paths"]];
            else if(selector ==@selector(saveContracts:))
                [self saveContracts:[response valueForKey:@"enhancedConstractsList"]];
            else if(selector ==@selector(saveHouses:))
                [self saveHouses:[response valueForKey:@"houses"]];
            else if(selector ==@selector(saveHajCompanies:))
                [self saveHajCompanies:[response valueForKey:@"hajjCompanies"]];
            else if(selector ==@selector(saveHajMissions:))
                [self saveHajMissions:[response valueForKey:@"hajjMissions"]];
            else if(selector ==@selector(saveNationalities:))
                [self saveNationalities:[response valueForKey:@"nationalities"]];
        }
        
     }];
     [serviceManager setErrorHandler:^(NSDictionary *dict , SEL selector){
         SyncedServices=TOTAL_SERVICES_TO_BE_SYNCED;
         [self checkIFSyncingFinished];
     }];
}
-(void)saveNationalities:(NSArray *)data{
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_NATIONALITIES withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync Nationalities Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)saveHouses: (NSArray *)data{
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_HOUSES withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync HOUSES Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)saveHajCompanies: (NSArray *)data{
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_HAJ_COMPANIES withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync HAJ COMPANIES Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)saveHajMissions: (NSArray *)data{
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_HAJ_MISSION withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync HAJ MISSIONS Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}

-(void)savePorts: (NSArray *)data{
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_PORTS withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync PORTS Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)savePaths: (NSArray *)data{
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_PATHS withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync PATHS Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)saveContracts: (NSArray *)data{
    if (data.count>0) {
       
        [DBManager refreshDataForEntity:ENTITY_CONTRACTS withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync CONTRACTS Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)saveTransportCompanies: (NSArray *)data{
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_TRANSPORT_COMPANIES withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync TRANSPORT COMPANIES Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}

-(void) saveCountries :(NSArray*) data {
    if (data.count>0) {
        
        [DBManager refreshDataForEntity:ENTITY_COUNTRIES withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
        }];
        
    }
    NSLog(@">>>Sync Countries Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}

-(void) saveCities:(NSArray*) data {
    
        
        [DBManager refreshDataForEntity:ENTITY_CITIES withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
            
            
        }];
        
    
    NSLog(@">>>Sync Cities Done");
    
    SyncedServices ++;
    
     [self checkIFSyncingFinished];
}
-(void)saveHotels:(NSArray *)data{
    [DBManager refreshDataForEntity:ENTITY_HOTELS withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
        
    }];
    
    
    NSLog(@">>>Sync Hotels Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)saveFlights:(NSArray *)data{
    [DBManager refreshDataForEntity:ENTITY_FLIGHTS withData:data withContext:appDelegate.persistentContainer.viewContext withBlock:^(BOOL status){
        
    }];
    
    NSLog(@">>>Sync Flights Done");
    
    SyncedServices ++;
    
    [self checkIFSyncingFinished];
}
-(void)checkIFSyncingFinished{
    if(SyncedServices==TOTAL_SERVICES_TO_BE_SYNCED){
        [[NSNotificationCenter defaultCenter]postNotificationName:GENERAL_ACTION_SYNC object:nil];
    }
}


@end
