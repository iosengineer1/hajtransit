//
//  LookupsManager.h
//  tafweej
//
//  Created by MOHAMMED TAQI AHMED MIR on 8/17/16.
//  Copyright © 2016 com.sejeltech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServicesManager.h"
#import "WebServicesURLs.h"


@interface LookupsManager : NSObject


@property (nonatomic,copy) completionBlock _Nullable completionHandler;
@property (nonatomic,copy) errorBlock _Nullable errorHandler;
@property (nonatomic)BOOL isLoading;


+(nonnull LookupsManager*)sharedLookupsHandler;
-(void)startSyncing;
@end
