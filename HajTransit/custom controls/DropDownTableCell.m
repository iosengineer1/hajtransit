//
//  DropDownTableCell.m
//  InternalUmrah
//
//  Created by Mohammed Mir on 2/18/18.
//  Copyright © 2018 Mohammed Mir. All rights reserved.
//

#import "DropDownTableCell.h"

@implementation DropDownTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(IBAction)clearText:(id)sender{
    [_delegate requestedForTextClearance:_indexPath forField:_fieldName];
}

@end
