//
//  DropDownTableCell.h
//  InternalUmrah
//
//  Created by Mohammed Mir on 2/18/18.
//  Copyright © 2018 Mohammed Mir. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol DropDownTableCellDelegate;
@interface DropDownTableCell : UITableViewCell{
    
}
@property(nonatomic,strong)id<DropDownTableCellDelegate>delegate;
@property(weak, nonatomic)IBOutlet UITextField *textField;
@property(weak, nonatomic)IBOutlet UILabel *cellHeaderLabel;
@property(weak, nonatomic)IBOutlet UILabel *cellValidationStatusLabel;
@property(weak, nonatomic)IBOutlet UIImageView *iconImageView;
@property(weak, nonatomic)IBOutlet UIImageView *cellDirectionImageView;
@property(nonatomic,strong)NSIndexPath *indexPath;
@property(weak, nonatomic) IBOutlet UIButton *clearTextButton;
@property(copy, nonatomic)NSString *fieldName;

-(IBAction)clearText:(id)sender;

@end

@protocol DropDownTableCellDelegate <NSObject>
@optional
-(void)rowSelectedAtIndex:(NSIndexPath *)indexPath;
-(void)requestedForTextClearance:(NSIndexPath *)indexPath forField:(NSString *)fieldName;

@end
