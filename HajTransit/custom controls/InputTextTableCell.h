//
//  InputTextTableCell.h
//  InternalUmrah
//
//  Created by Mohammed Mir on 2/18/18.
//  Copyright © 2018 Mohammed Mir. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol InputTextTableCellDelegate;
@interface InputTextTableCell : UITableViewCell<UITextFieldDelegate>{
    
}
@property(nonatomic,strong)id<InputTextTableCellDelegate>delegate;
@property(weak, nonatomic)IBOutlet UITextField *textField;
@property(weak, nonatomic)IBOutlet UILabel *cellHeaderLabel;
@property(weak, nonatomic)IBOutlet UILabel *cellValidationStatusLabel;
@property(weak, nonatomic)IBOutlet UIImageView *iconImageView;
@property(weak, nonatomic)IBOutlet UIImageView *cellDirectionImageView;
@property(nonatomic,strong)NSIndexPath *indexPath;
@property(copy, nonatomic)NSString *fieldName;
@property(weak, nonatomic) IBOutlet UIButton *clearTextButton;


-(IBAction)qrOptionIsSelected:(id)sender;

@end

@protocol InputTextTableCellDelegate <NSObject>
@optional
-(void)textFieldDidFinishEditing:(UITextField *)textField forCell:(InputTextTableCell *)cell;
-(void)textFieldDidBeginEditing:(UITextField *)textField;
-(void)fetchDataForID:(UITextField *)textField;
-(void)fetchDataForQRCode:(NSIndexPath *)indexPath;

@end
