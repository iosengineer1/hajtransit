//
//  InputTextTableCell.m
//  InternalUmrah
//
//  Created by Mohammed Mir on 2/18/18.
//  Copyright © 2018 Mohammed Mir. All rights reserved.
//

#import "InputTextTableCell.h"

@implementation InputTextTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_delegate textFieldDidBeginEditing:textField];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    [_delegate textFieldDidFinishEditing:textField forCell:self];
}
-(void)setValidationTextWithStatus:(BOOL)status forField:(UITextField *)txtField{
    if(txtField.text.length!=0)
    _cellValidationStatusLabel.text = status==NO?NSLocalizedString(@"Invalid", nil):@"";
    else
    _cellValidationStatusLabel.text = status==NO?NSLocalizedString(@"Required", nil):@"";

}
-(IBAction)textDidChange:(UITextField*)textField{
    textField.text = [[UtilityHandler sharedHandler]replaceArabicNumber:textField.text];
    BOOL status = NO;
    if([_fieldName isEqualToString:MOBILE_NUMBER_FIELD]){
        if(![textField.text hasPrefix:@"0"]&&textField.text.length>=9){
            status = YES;
        }
    }
    else if([_fieldName isEqualToString:ID_NUMBER_FIELD]){
        if(textField.text.length>=10){
            status = YES;
        }
    }
    else if([_fieldName isEqualToString:EMAIL_FIELD]){
        if([self isValidEmail:textField.text] && textField.text.length>=5){
            status = YES;
        }
    }
    
    else if([_fieldName isEqualToString:FULL_TEXT]||[_fieldName isEqualToString:USER_NAME_FIELD]||[_fieldName isEqualToString:PASSWORD_FIELD]||[_fieldName isEqualToString:DRIVER_ID_FIELD]||[_fieldName isEqualToString:NAQABA_ID_FIELD]){
        if(textField.text.length>0){
            status = YES;
        }
    }
    //For No validation messages to be shown
    else if ([_fieldName isEqualToString:PARKING_NUMBER_FIELD]){
        status = YES;
    }
    
    [self setValidationTextWithStatus:status forField:textField];
    
}
-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    string = [[UtilityHandler sharedHandler]replaceArabicNumber:string];
    /*  limit to only numeric characters  */
    if([string isEqualToString:@""])
        return YES;
    if([_fieldName isEqualToString:PARKING_NUMBER_FIELD]){
        if(textField.text.length>9)
            return NO;
    }
    if([_fieldName isEqualToString:MOBILE_NUMBER_FIELD]){
        if(textField.text.length>9)
         return NO;
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if (![myCharSet characterIsMember:c]) {
            return NO;
        }
    }
    }
    else if([_fieldName isEqualToString:ID_NUMBER_FIELD]){
        if(textField.text.length>14)
            return NO;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c]) {
                return NO;
            }
        }
    }
    
    
    return YES;
}
-(IBAction)qrOptionIsSelected:(id)sender{
    if([_delegate respondsToSelector:@selector(fetchDataForQRCode:)]){
        [_delegate fetchDataForQRCode:_indexPath];
    }
}

@end
