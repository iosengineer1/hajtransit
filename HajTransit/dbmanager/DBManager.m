//
//  DBManager.m
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/27/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import "DBManager.h"



@implementation DBManager

+(NSArray *)dataForEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setShouldRefreshRefetchedObjects:YES];
    [fetchRequest setEntity:entityDesc];
    fetchRequest.returnsDistinctResults = YES;
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    return result;
}
+(NSArray *)coreDataForEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    [fetchRequest setShouldRefreshRefetchedObjects:YES];
    [fetchRequest setEntity:entityDesc];
    fetchRequest.returnsDistinctResults = YES;
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    return result;
}


+(NSArray *)getAllDistinctAttributesForKey:(NSString *)key fromEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setResultType:NSDictionaryResultType];
    NSAttributeDescription *groupId = [entityDesc.propertiesByName objectForKey:key];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    fetchRequest.returnsDistinctResults = YES;
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:groupId, nil]];
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"date"
                                        ascending:NO];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    return result;
    
}
+(NSArray *)fetchRequestForEntity:(NSString *)entity predicate:(NSPredicate *)predicate fetchLimit:(BOOL)limit sortDescriptor:(NSSortDescriptor *)sort withContext:(NSManagedObjectContext *)context{
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (limit)
        [request setFetchLimit:1];
    if(predicate!=nil)
        [request setPredicate:predicate];
    if(sort!=nil)
        [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    [request setShouldRefreshRefetchedObjects:YES];
    request.returnsDistinctResults = YES;
    NSArray *result = [context executeFetchRequest:request error:&error];
    
    return result;
}

+(NSArray *)getAllDistinctAttributesForKey:(NSString *)key fromEntity:(NSString *)entity predicate:(NSPredicate *)predicate withContext:(NSManagedObjectContext *)context{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setResultType:NSDictionaryResultType];
    NSAttributeDescription *groupId = [entityDesc.propertiesByName objectForKey:key];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    fetchRequest.returnsDistinctResults = YES;
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:groupId, nil]];
    if(predicate!=nil)
        [fetchRequest setPredicate:predicate];
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    return result;
    
}

+(NSArray *)fetchRequestForEntity:(NSString *)entity predicate:(NSPredicate *)predicate fetchLimit:(BOOL)limit sortDescriptor:(NSSortDescriptor *)sort{
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (limit)
        [request setFetchLimit:1];
    if(predicate!=nil)
        [request setPredicate:predicate];
    if(sort!=nil)
        [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    [request setShouldRefreshRefetchedObjects:YES];
    request.returnsDistinctResults = YES;
    NSArray *result = [appDelegate.persistentContainer.viewContext executeFetchRequest:request error:&error];
    
    return result;
}
+(NSArray *)fetchRequestForEntity:(NSString *)entity predicate:(NSPredicate *)predicate fetchLimit:(BOOL)limit sortDescriptor:(NSSortDescriptor *)sort dictionaryType:(BOOL)type{
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (limit)
        [request setFetchLimit:1];
    if(predicate!=nil)
        [request setPredicate:predicate];
    if(sort!=nil)
        [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    if(type)
        [request setResultType:NSDictionaryResultType];
    [request setShouldRefreshRefetchedObjects:YES];
    request.returnsDistinctResults = YES;
    NSArray *result = [appDelegate.persistentContainer.viewContext executeFetchRequest:request error:&error];
    
    return result;
}

+(NSArray *)fetchRequestForEntity:(NSString *)entity predicate:(NSPredicate *)predicate fetchLimit:(BOOL)limit sortDescriptors:(NSArray *)sort dictionaryType:(BOOL)type{
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (limit)
        [request setFetchLimit:1];
    if(predicate!=nil)
        [request setPredicate:predicate];
    if(sort!=nil)
        [request setSortDescriptors:sort];
    if(type)
        [request setResultType:NSDictionaryResultType];
    [request setShouldRefreshRefetchedObjects:YES];
    request.returnsDistinctResults = YES;
    NSArray *result = [appDelegate.persistentContainer.viewContext executeFetchRequest:request error:&error];
    
    return result;
}



+(NSArray *)getDataForEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    //[fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setShouldRefreshRefetchedObjects:YES];
    [fetchRequest setEntity:entityDesc];
    fetchRequest.returnsDistinctResults = YES;
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    }
    
    return result;
}

+(NSArray *)getRecordsForEntity:(NSString *)entity forYear:(int)season month:(int)monthPeriod withContext:(NSManagedObjectContext *)context{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    [fetchRequest setResultType:NSDictionaryResultType];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(season==%d)AND(month==%d)",season,monthPeriod];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey: @"month" ascending: YES];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:descriptor]];
    [fetchRequest setEntity:entityDesc];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    }
    
    return result;
    
    
}

+ (NSFetchRequest*) fetchRequestForSingleInstanceOfEntity:(NSString*)entityName groupedBy:(NSString*)attributeName
{
    __block NSMutableSet *uniqueAttributes = [NSMutableSet set];
    
    NSPredicate *filter = [NSPredicate predicateWithBlock:^(id evaluatedObject, NSDictionary *bindings) {
        if( [uniqueAttributes containsObject:[evaluatedObject valueForKey:attributeName]] )
            return NO;
        
        [uniqueAttributes addObject:[evaluatedObject valueForKey:attributeName]];
        return YES;
    }];
    
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:entityName];
    req.predicate = filter;
    
    return req;
}

+ (NSArray*) fetchOneInstanceOfEntity:(NSString*)entityName groupedBy:(NSString*)attributeName withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *req = [self fetchRequestForSingleInstanceOfEntity:entityName groupedBy:attributeName];
    
    // perform fetch
    NSError *fetchError = nil;
    NSArray *fetchResults = [context executeFetchRequest:req error:&fetchError];
    
    // fetch results
    if( !fetchResults ) {
        // Handle error ...
        return nil;
    }
    
    return fetchResults;
}

+(NSArray *)getDistinctObjects:(NSString *)entity withContext:(NSManagedObjectContext *)context{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setReturnsDistinctResults:YES];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    }
    
    
    return result;
}

+(void)deleteDataForEntity:(NSString *)entity withCotext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *object in fetchedObjects)
    {
        [context deleteObject:object];
    }
    [appDelegate.persistentContainer.persistentStoreCoordinator performBlockAndWait:^{
        NSError * error = nil;
        if (![context save: &error])
            NSLog(@"Couldn't save: %@", [error localizedDescription]
                  );
        
        else
            NSLog(@"Deleted successfully");
    }];
    
}
+(void)refreshDataForEntity:(NSString *)entity withData:(NSArray *)data withContext:(NSManagedObjectContext *)context withBlock:(onCompleted)block{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init] ;
    [fetchRequest setReturnsObjectsAsFaults:NO];
    NSEntityDescription *aboutEntity = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    [fetchRequest setEntity:aboutEntity];
    NSMutableArray *result = [[context executeFetchRequest:fetchRequest error:&error]mutableCopy];
    if (result.count>0)
        [self deleteDataForEntity:entity withCotext:context];
    __block BOOL status = false ;
    for (NSDictionary *record in data) {
        
    
        if ([entity isEqualToString:ENTITY_COUNTRIES]){
            Countries *entityCountry  = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
           
            entityCountry.iD = NULL_TO_NIL([record valueForKey:@"countryId"]);
            entityCountry.nameAr = NULL_TO_NIL([record valueForKey:@"countryNameAr"]);
            entityCountry.nameEn = NULL_TO_NIL([record valueForKey:@"countryNameLa"]);
            
        }
        else if ([entity isEqualToString:ENTITY_CITIES]){
            Cities *entityCity  = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
            entityCity.iD = NULL_TO_NIL([record valueForKey:@"cityId"]);
            entityCity.countryId =NULL_TO_NIL([record valueForKey:@"countryId"]);
            entityCity.nameAr = CHECK_NULL_STRING([record valueForKey:@"cityNameAr"]);
            entityCity.nameEn = CHECK_NULL_STRING([record valueForKey:@"cityNameLa"]);
            
        }
          else if ([entity isEqualToString:ENTITY_HOTELS]){
              
//            Hotel *aHotel  = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
//              aHotel.iD = NULL_TO_NIL([record valueForKey:@"hotelId"]);
//              aHotel.nameAr = NULL_TO_NIL([record valueForKey:@"hotelNameAr"]);
//              aHotel.hotelOwnerName = NULL_TO_NIL([record valueForKey:@"hotelOwnerName"]);
//              aHotel.hotelMohRehNo = NULL_TO_NIL([record valueForKey:@"hotelMohRehNo"]);
//              aHotel.hotelSctaRegNo = NULL_TO_NIL([record valueForKey:@"hotelSctaRegNo"]);
//              aHotel.hotelCity = NULL_TO_NIL([record valueForKey:@"hotelCity"]);
//              aHotel.hotelClassification = NULL_TO_NIL([record valueForKey:@"hotelClassification"]);
//              aHotel.hotelPhone = NULL_TO_NIL([record valueForKey:@"hotelPhone"]);
//              aHotel.hotelFax = NULL_TO_NIL([record valueForKey:@"hotelFax"]);
//              aHotel.hotelEmail = NULL_TO_NIL([record valueForKey:@"hotelEmail"]);
//              aHotel.hotelManagerName = NULL_TO_NIL([record valueForKey:@"hotelManagerName"]);
//              aHotel.hotelManagerMobile = NULL_TO_NIL([record valueForKey:@"hotelManagerMobile"]);
//              aHotel.nameEn = NULL_TO_NIL([record valueForKey:@"hotelNameLa"]);
//              aHotel.longitude = @([NULL_TO_NIL([record valueForKey:@"hotelLongitude"]) doubleValue]);
//              aHotel.latitude = @([NULL_TO_NIL([record valueForKey:@"hotelLatitude"]) doubleValue]);
//              
//              aHotel.hotelState = NULL_TO_NIL([record valueForKey:@"hotelState"]);
//              aHotel.hotelGdocdRegNoExpiryHij = [NULL_TO_NIL([record valueForKey:@"hotelGdocdRegNoExpiryHij"]) stringValue];
//              
//              aHotel.hotelGdocdRegNo =NULL_TO_NIL([record valueForKey:@"hotelGdocdRegNo"]);
//              
          }
        

    }
    
    [appDelegate.persistentContainer.persistentStoreCoordinator performBlockAndWait:^{
        NSError * error = nil;
        if (![context save:&error]) {
            NSLog(@"Couldn't update: %@", [error localizedDescription]);
            status = false;
        } else{
            NSLog(@"Updated successfully");
            status = true;
            ;
        }
    }];
    
    block(status);
}

+(NSString*) getStringDate:(NSString *) dateString {
    
    double timestamp  = ([dateString doubleValue] / 1000);
    NSDate *date=[NSDate dateWithTimeIntervalSince1970:timestamp];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString  *dateStringDisplay = [formatter stringFromDate:date];
    
    return dateStringDisplay;
    
    
}

+(NSDate *) getDate:(NSString *) dateString {
    
    double timestamp  = ([dateString doubleValue] / 1000);
    NSDate *date=[NSDate dateWithTimeIntervalSince1970:timestamp];
    
    return date;
    
    
}




@end
