//
//  DBManager.h
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/27/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UtilityHandler.h"

#define tripType_leave @"LEAVE"
#define tripType_arrive @"ARRIVE"

typedef void (^onListLoadCompleted)(id obj);
typedef void (^onDictLoadCompleted)(NSMutableDictionary *dict);
typedef void (^onCompleted)(BOOL status);
@interface DBManager : NSObject


+(NSArray *)dataForEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context;
+(NSArray *)getDataForEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context;
+(BOOL)dbContainsData;
+(NSArray *)getRecordsForEntity:(NSString *)entity forYear:(int)season month:(int)monthPeriod withContext:(NSManagedObjectContext *)context;

+(NSArray *)coreDataForEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context;
+(NSArray *)getDistinctObjects:(NSString *)entity withContext:(NSManagedObjectContext *)context;


+ (NSArray*) fetchOneInstanceOfEntity:(NSString*)entityName groupedBy:(NSString*)attributeName withContext:(NSManagedObjectContext *)context;
+(NSArray *)getAllDistinctAttributesForKey:(NSString *)key fromEntity:(NSString *)entity withContext:(NSManagedObjectContext *)context;
+(void)refreshDataForEntity:(NSString *)entity withData:(NSArray *)data withContext:(NSManagedObjectContext *)context withBlock:(onCompleted)block;
+(NSArray *)fetchRequestForEntity:(NSString *)entity predicate:(NSPredicate *)predicate fetchLimit:(BOOL)limit sortDescriptor:(NSSortDescriptor *)sort dictionaryType:(BOOL)type;
+(NSArray *)fetchRequestForEntity:(NSString *)entity predicate:(NSPredicate *)predicate fetchLimit:(BOOL)limit sortDescriptors:(NSArray *)sort dictionaryType:(BOOL)type;

@end
