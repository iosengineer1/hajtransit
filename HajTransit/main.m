//
//  main.m
//  HajTransit
//
//  Created by Mohammed Mir on 05/06/2018.
//  Copyright © 2018 Mohammed Mir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
