//
//  Utility.h
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/22/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//2B5D6A

#define CHECK_NULL_STRING(str) ([str isKindOfClass:[NSNull class]] || !str)?@"":str

@interface UtilityHandler : NSObject
+(UtilityHandler*)sharedHandler;
- (void)saveObject:(NSObject *)object ForKey:(NSString *)key;
- (id)getObjectForKey:(NSString *)key;
-(void)setUserLoggedIn;
-(BOOL)isUserLoggedIn;
-(void)setUserLoggedOut;
-(void)updateUserProfileStatus:(BOOL)status;
-(void)showAlertForController:(UIViewController *)vc withMessage:(NSString *)msg;
-(void)showAlertForController:(UIViewController *)vc withMessage:(NSString *)msg withTitle
:(NSString *)title;
-(void)showAlertForController:(UIViewController *)viewController message:(NSString *)msg
                        title:(NSString *)title withButtonTitle:(NSString *)btTitle  popViewController:(BOOL)pop;
-(void)showAlertForController:(UIViewController *)viewController message:(NSString *)msg title:(NSString *)title withButtonTitle:(NSString *)btTitle withBlock:(void (^)(void))completion;
-(NSString*)replaceArabicNumber:(NSString *)numericString;
- (void)showHudWithMessage:(NSString *)msg;
- (void)hideProgressHUD;
+(void)showActivity:(UIView *)activityView fromView:(UIView *)fromView;
+(void)hideActivity:(UIView *)activityView fromView:(UIView *)fromView;
-(BOOL)checkPhone:(NSString *)phoneNumber;
-(BOOL)checkEmail:(NSString *)email;
-(BOOL)selectedImage:(UIImage *)image1 isEqualTo:(UIImage *)image2 ;
-(NSString *)manifestType:(NSInteger)type;
-(NSString *)getTotalHajis:(NSString *)adultCount childCount:(NSString *)childCount infantCount:(NSString *)infantCount;
-(void)setUserRoleWithStatus:(BOOL)status;
-(BOOL)isUserAdmin;
-(BOOL)isUserAllowedToModifyManifestWithClosedDate:(NSString *)closedDate;
- (NSString *)documentsDirectory;
-(UIImage*)compositImageFrom:(UIImage*)firstImage withImage:(UIImage*)secondImage viewFrame:(CGRect)frame;
-(NSString *)dateStringFromDate:(NSDate *)date;
-(NSString *)getCurrentHijriYearFromDate:(NSDate *)date;
-(void)enableCell:(UITableViewCell *)cell;
-(void)disableCell:(UITableViewCell *)cell;
@end

