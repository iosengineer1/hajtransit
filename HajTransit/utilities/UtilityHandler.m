//
//  Utility.m
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/22/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import "UtilityHandler.h"

static UtilityHandler *sharedHandler = nil;

@implementation UtilityHandler

+(UtilityHandler*)sharedHandler
{
    @synchronized([UtilityHandler class])
    {
        if (!sharedHandler)
            sharedHandler = [[self alloc] init];
    }
    
    return sharedHandler;
}


+(id)alloc
{
    @synchronized([UtilityHandler class])
    {
        NSAssert(sharedHandler== nil, @"Attempted to allocate a second instance of a singleton.");
        sharedHandler = [super alloc];
        return sharedHandler;
    }
}

-(id)init {
    self = [super init];
    if (self != nil) {
        
    }
    return self;
    
}

-(void)setUserLoggedIn{
    [def setBool:YES forKey:IS_USER_LOGGED_IN];
    
    [def synchronize];
}
-(void)setUserRoleWithStatus:(BOOL)status{
    [def setBool:status forKey:IS_USER_ADMIN];
}
-(BOOL)isUserAdmin{
    if ([def valueForKey:IS_USER_ADMIN] == nil)
        return NO;
    
    if ([def boolForKey:IS_USER_ADMIN]==NO)
        return NO;
    
    return YES;
}
-(BOOL)isUserLoggedIn{
    if ([def valueForKey:IS_USER_LOGGED_IN] == nil)
        return NO;
    
    if ([def boolForKey:IS_USER_LOGGED_IN]==NO)
        return NO;
    
    return YES;
}
- (void)saveObject:(NSObject *)object ForKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getObjectForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}





-(void)showAlertForController:(UIViewController *)vc withMessage:(NSString *)msg withTitle:(NSString *)title
{
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:SejelLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
    }];
    [alert addAction:okAction];
    
    [vc presentViewController:alert animated:YES completion:nil];
    
}
-(void)showAlertForController:(UIViewController *)viewController message:(NSString *)msg
                        title:(NSString *)title withButtonTitle:(NSString *)btTitle  popViewController:(BOOL)pop
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:btTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        if (pop)
            [viewController.navigationController popViewControllerAnimated:YES];
        
    }];
    [alert addAction:okAction];
    [viewController presentViewController:alert animated:YES completion:nil];
    
}
-(void)setUserLoggedOut{
    
    [def setBool:NO forKey:IS_USER_LOGGED_IN];
    [KeyChain deleteEntryForKey:K_SAUTH_USER_NAME];
    [KeyChain deleteEntryForKey:K_SAUTH_PASSWORD];
    [def synchronize];
    [appDelegate gotoLoginView];
    
}
-(void)showAlertForController:(UIViewController *)viewController message:(NSString *)msg title:(NSString *)title withButtonTitle:(NSString *)btTitle withBlock:(void (^)(void))completion
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:btTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
      
        completion();
        
    }];
    [alert addAction:okAction];
    [viewController presentViewController:alert animated:YES completion:nil];
    
}
-(void)showAlertForController:(UIViewController *)vc withMessage:(NSString *)msg
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:SejelLocalizedString(@"Alert", nil)
                                  message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:SejelLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
    }];
    [alert addAction:okAction];
    
    [vc presentViewController:alert animated:YES completion:nil];
    
}
- (NSString *)documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return paths[0];
}
-(NSString *)getCurrentHijriYearFromDate:(NSDate *)date{
    
    NSDateComponents *hijriComponents;
    NSDate *dt;
    
    NSCalendar *hijriCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIslamicUmmAlQura];
    NSDate *_date = date==nil?[NSDate date]:date;
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *gregorianComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:_date];
    
    
    gregorianComponents.day = [gregorianComponents day];
    gregorianComponents.month = [gregorianComponents month];
    gregorianComponents.year = [gregorianComponents year];
    
    
    dt = [gregorianCalendar dateFromComponents:gregorianComponents];
    
    
    hijriComponents  = [hijriCalendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear)
                                        fromDate:dt];
    
    
    return [NSString stringWithFormat:@"%ld",(long)[hijriComponents year]];
}
-(NSString*)replaceArabicNumber:(NSString *)numericString {
    NSMutableString *s = [NSMutableString stringWithString:numericString];
    NSString *arabic = @"١٢٣٤٥٦٧٨٩٠";
    NSString *western = @"1234567890";
    for (uint i = 0; i<arabic.length; i++) {
        NSString *a = [arabic substringWithRange:NSMakeRange(i, 1)];
        NSString *w = [western substringWithRange:NSMakeRange(i, 1)];
        [s replaceOccurrencesOfString:a withString:w
                              options:NSCaseInsensitiveSearch
                                range:NSMakeRange(0, s.length)];
    }
    return [NSString stringWithString:s];
}

-(BOOL)isUserAllowedToModifyManifestWithClosedDate:(NSString *)closedDate{
    BOOL isAllowed = NO;
    
    NSDate *crDate = [NSDate date];
    NSDate *clDate = [NSDate dateWithTimeIntervalSince1970:([closedDate longLongValue]/1000)];

    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"dd-MM-yyyy HH:mm a"];
    
    if ([crDate timeIntervalSinceDate:clDate] <= 3600)
    {
        isAllowed = YES;
    }
    
    return isAllowed;
}
-(void)showHudWithMessage:(NSString *)msg{
    
    MBProgressHUD* hud =   [MBProgressHUD showHUDAddedTo:appDelegate.window animated:true];
    
    hud.animationType = MBProgressHUDAnimationZoom;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.backgroundView.color =  [UIColor colorWithWhite:0.f alpha:.4f];
    hud.label.text = msg;
    
}
-(NSString *)manifestType:(NSInteger)type{
    NSString *manifest = @"";
    switch (type) {
        case 1:
            manifest = SejelLocalizedString(@"NORMAL", nil);
            break;
            
        default:
            manifest = SejelLocalizedString(@"LAND TRIP", nil);
            break;
    }
    return manifest;
}
-(void)hideProgressHUD{
    
    [MBProgressHUD hideHUDForView:appDelegate.window animated:true];
    
}
+(void)showActivity:(UIView *)activityView fromView:(UIView *)fromView{
    activityView.hidden = NO;
    activityView.backgroundColor = [UIColor blackColor];
    UIActivityIndicatorView *actView = (UIActivityIndicatorView *)[activityView viewWithTag:100];
    if ([actView respondsToSelector:@selector(startAnimating)]) {
        [actView startAnimating];
    }
    [fromView bringSubviewToFront:activityView];
}
+(void)hideActivity:(UIView *)activityView fromView:(UIView *)fromView{
    activityView.backgroundColor = [UIColor clearColor];
    UIActivityIndicatorView *actView = (UIActivityIndicatorView *)[activityView viewWithTag:100];
    if ([actView respondsToSelector:@selector(stopAnimating)]) {
        [actView stopAnimating];
    }
    activityView.hidden = YES;
    [fromView sendSubviewToBack:activityView];
}
-(BOOL)checkEmail:(NSString *)email{
    BOOL isValid = NO;
    
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    isValid =  [emailPredicate evaluateWithObject:email];
    return isValid;
}

-(BOOL)checkPhone:(NSString *)phoneNumber{
    BOOL isValid = NO;
    NSString *phoneRegex = @"[235689][0-9]([0-9]{3})?";
    NSPredicate *phonePredicate = [ NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    isValid = [phonePredicate evaluateWithObject:phoneNumber];
    return isValid;
}
-(BOOL)selectedImage:(UIImage *)image1 isEqualTo:(UIImage *)image2 {
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqualToData:data2];
}
-(NSString *)getTotalHajis:(NSString *)adultCount childCount:(NSString *)childCount infantCount:(NSString *)infantCount{
    NSString *totalStr = @"";
    NSString *adult = adultCount!=nil?[NSString stringWithFormat:@"%@",adultCount]:@"0";
    NSString *child = childCount!=nil?[NSString stringWithFormat:@"%@",childCount]:@"0";
    NSString *infant = infantCount!=nil?[NSString stringWithFormat:@"%@",infantCount]:@"0";
    NSInteger total = [adult integerValue] + [child integerValue] + [infant integerValue];
    totalStr = [@(total)stringValue];
    return totalStr;
}
- (UIImage*)compositImageFrom:(UIImage*)firstImage withImage:(UIImage*)secondImage viewFrame:(CGRect)frame {
    UIImage *image = nil;
    
    CGSize newImageSize = CGSizeMake(MAX(firstImage.size.width, secondImage.size.width), MAX(firstImage.size.height, secondImage.size.height));
    
    UIGraphicsBeginImageContext(newImageSize);
    //roundf((newImageSize.width-firstImage.size.width)/2)
    //roundf((newImageSize.height-firstImage.size.height)/2)
    
    [firstImage drawAtPoint:CGPointMake([def boolForKey:IS_LANGUAGE_ENGLISH]?appDelegate.window.frame.size.width-frame.size.width:20,frame.size.height)];
    /*[secondImage drawAtPoint:CGPointMake(roundf((newImageSize.width-secondImage.size.width)/2),
     roundf((newImageSize.height-secondImage.size.height)/2))];*/
    [secondImage drawAtPoint:CGPointMake([def boolForKey:IS_LANGUAGE_ENGLISH]?0:20,20)];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
-(void)disableCell:(UITableViewCell *)cell{
    cell.userInteractionEnabled = NO;
    for(UIView *view in [cell.contentView subviews]){
        view.alpha = 0.5;
    }
}
-(void)enableCell:(UITableViewCell *)cell{
    cell.userInteractionEnabled = YES;
    for(UIView *view in [cell.contentView subviews]){
        view.alpha = 1;
    }
}


@end

