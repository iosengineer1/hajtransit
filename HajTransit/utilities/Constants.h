//
//  Constants.h
//  MRZ Scanner
//
//  Created by Mohammed Mir on 01/04/2018.
//  Copyright © 2018 Sejel Technology. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define CHECK_NULL_STRING(str) ([str isKindOfClass:[NSNull class]] || !str)?@"":str
#define CHECK_NULL_TEXT(str) ([str isEqualToString:@"<null>"] || !str)?@"":str

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })
#define def [NSUserDefaults standardUserDefaults]
#define Main_Story ([UIStoryboard storyboardWithName:@"Main" bundle:nil])
#define KeyChain ([A0SimpleKeychain keychain])


#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication]delegate])

#define APP_COLOR ([UIColor colorWithRed:25.0f/255.0f green:136.0f/255.0f blue:88.0f/255.0f alpha:1])
#define RED_COLOR ([UIColor colorWithRed:156.0f/255.0f green:43.0f/255.0f blue:60.0f/255.0f alpha:1])
#define GREEN_COLOR ([UIColor colorWithRed:43.0f/255.0f green:116.0f/255.0f blue:48.0f/255.0f alpha:1])
#define MANIFEST_INCOMPLETE_COLOR ([UIColor colorWithRed:255.0f/255.0f green:165.0f/255.0f blue:0.0f/255.0f alpha:1])

#define KeyChain ([A0SimpleKeychain keychain])

#define BAR_CODE_FONT [UIFont fontWithName:@"FRE3OF9EXTENDED" size:15]

#define IS_USER_LOGGED_IN @"IS_USER_LOGGED_IN"
#define IS_USER_ADMIN @"isAdmin"

#define K_SAUTH_USER_NAME  @"userName"
#define K_SAUTH_PASSWORD  @"password"
#define K_USER_ID @"userId"
#define K_USER_PORTS @"repPorts"

#define ENTITY_COUNTRIES  @"Countries"
#define ENTITY_HOTELS    @"Hotel"
#define ENTITY_CITIES    @"Cities"
#define ENTITY_FLIGHTS @"Flight"

#define ENTITY_PATHS @"Paths"
#define ENTITY_CONTRACTS @"Contracts"
#define ENTITY_PORTS @"Ports"
#define ENTITY_TRANSPORT_COMPANIES @"TransportCompanies"
#define ENTITY_HOUSES @"Houses"
#define ENTITY_HAJ_COMPANIES @"HajCompanies"
#define ENTITY_HAJ_MISSION @"HajMission"
#define ENTITY_NATIONALITIES @"Nationalities"




#define GENERAL_ACTION_SYNC @"Sync"
#define LANG_EN @"en"
#define LANG_AR @"ar"
#define IS_LANGUAGE_ENGLISH @"LANG_CODE"
#define SELECTED_LANGUAGE @"LANGUAGE"

#define DROP_DOWN_ENTITY_TYPE  @"DROP_DOWN_ENTITY_TYPE"
#define DROP_DOWN_PREV_SELECTION @"DROP_DOWN_PREV_SELECTION"
#define DROP_DOWN_TITLE @"DROP_DOWN_TITLE"
#define DROP_DOWN_DEPENDANCY @"DROP_DOWN_DEPENDANCY"

#define MOBILE_NUMBER_FIELD @"mobile"
#define ID_NUMBER_FIELD @"ID"
#define NATIONALITY_FIELD @"nationality"
#define EMAIL_FIELD @"email"
#define FULL_TEXT @"fullname"
#define COUNTRY_FIELD @"country"
#define CITY_FIELD @"city"
#define PORT_FIELD @"port"
#define PATH_FIELD @"path"
#define CONTRACT_FIELD @"contract"
#define USER_NAME_FIELD @"username"
#define PASSWORD_FIELD @"password"
#define HAJ_MISSION_FIELD @"mission"
#define TRANSPORTATION_COMPANY_FIELD @"transportation"
#define HAJ_COMPANY_FIELD @"company"
#define HAJ_HOUSE_FIELD @"house"
#define HAJ_HOUSE_CONTRACT_FIELD @"housecontract"
#define DRIVER_ID_FIELD @"driverId"
#define NAME_FIELD @"name"
#define TRANSPORT_COMPANY_FIELD @"transportation"
#define NAQABA_ID_FIELD @"naqabaId"
#define PARKING_NUMBER_FIELD @"parking"
#define CHASIS_NO_FIELD @"chasisno"
#define PLATE_FIELD @"plate"
#define CAPACITY_FIELD @"capacity"


#define KERROR @"error"
#define KERROR_ID @"errorId"
#define KERROR_DESC @"errorDesc"
#define KERROR_DESC_EN @"errorDescLA"
#define FIELD_NAME @"fieldName"

#define SELECTED_PRINTER @"SELECTED_PRINTER"

#endif /* Constants_h */
