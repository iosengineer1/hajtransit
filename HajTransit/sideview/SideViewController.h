//
//  SideViewController.h
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/23/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilityHandler.h"


@interface SideViewController : UITableViewController
{
    __weak IBOutlet UIBarButtonItem *cancelButton;
}
@end
