//
//  SideViewController.m
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/23/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import "SideViewController.h"

#define  Image_TAG 1
#define Title_TAG 2
#define Printer_TAG 3
#define Printer_TYPE_TAG 4



@interface SideViewController (){
    NSArray *data;
    UIImageView *imgLoader;
    BOOL animateLoader;
    __weak IBOutlet UITableView *tblView;

};
-(IBAction)doneClicked:(id)sender;
@end

@implementation SideViewController



-(IBAction)doneClicked:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)refreshDone{
    [LookupsManager sharedLookupsHandler].isLoading = NO;
    [self stopSpin];
}
-(void) stopSpinAnimation{
    
    [imgLoader.layer removeAnimationForKey:@"SpinAnimation"];
}
-(void)viewWillAppear:(BOOL)animated{
    if([LookupsManager sharedLookupsHandler].isLoading)
    {
        animateLoader = NO;
        [self startSpin];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    [cancelButton setTitle:SejelLocalizedString(@"Cancel", nil)];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDone) name:GENERAL_ACTION_SYNC object:nil];
       data = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Sidemenu" ofType:@"plist"]];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Loader Functionality

- (void) spinWithOptions: (UIViewAnimationOptions) options {
    // this spin completes 360 degrees every 2 seconds
    [UIView animateWithDuration: 0.5f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         // view.layer.transform = CATransform3DMakeRotation(M_PI / 2.0, 0, 0, 1)
                         imgLoader.transform = CGAffineTransformRotate(imgLoader.transform, M_PI / 2);
                         
                         
                     }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (animateLoader) {
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
}


- (void) startSpin {
    if (!animateLoader) {
        animateLoader = YES;
        [LookupsManager sharedLookupsHandler].isLoading = YES;
        [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
    }
}

- (void) stopSpin {
    // set the flag to stop spinning after one last 90 degree increment
    animateLoader = NO;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *record = data[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[record valueForKey:@"cell"] forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:Image_TAG];
    UILabel *label = (UILabel *)[cell viewWithTag:Title_TAG];
    imageView.image = [UIImage imageNamed:[record valueForKey:@"image"]];
    if(indexPath.row==0)
        imgLoader = imageView;
    
    if (![def boolForKey:IS_LANGUAGE_ENGLISH]) {
        label.text = [record valueForKey:@"title_ar"];
    }
    else {
        label.text = [record valueForKey:@"title"];
    }
    
    
    
    // Configure the cell...
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
            if (animateLoader) {
                return;
            }
            [self startSpin];
            [[LookupsManager sharedLookupsHandler]startSyncing];
            break;
            
        case 1:
           // counter = 10;
          //  [self SelectLanguage];
            [self changeLanguage];
            break;
        case 2:
           //  [self performSegueWithIdentifier:@"PrinterListViewController" sender:nil];
           
            break;
        case 3:
            [self dismissViewControllerAnimated:YES completion:^{
                [[UtilityHandler sharedHandler]setUserLoggedOut];
            }];
            break;
            
        default:
            break;
    }
    
 
}


-(void)changeLanguage{
    
    [appDelegate relaunchApp];
     [self dismissViewControllerAnimated:YES completion:nil];
   
}
#pragma mark- EPOSPrinterDelgate

-(void)deviceHasBeenDetected{
    [tblView reloadData];
}
@end
