//
//  WebService.h
//  DOF
//
//  Created by Mir Taqi Ahmed on 5/5/15.
//  Copyright (c) 2015 TACME. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "InternetConnectionManager.h"

typedef void (^completionBlock)(id dict , SEL selector);
typedef void (^errorBlock)(id resp , SEL selector);



@interface WebServicesManager : NSObject<NSURLConnectionDelegate>
@property (nonatomic,copy) completionBlock completionHandler;
@property (nonatomic,copy) errorBlock errorHandler;


-(void)postRequestFor:(NSString *)serviceType parameters:(NSDictionary *)params withSelector:(SEL) selector;
-(void) fetchData:(NSString*) url  withParamters:(NSMutableDictionary*) parameters withSelector:(SEL) selector;
-(BOOL) internetAvailable;
-(void) postRequest:(NSString*) url  withParamters:(NSMutableDictionary*) parameters  andUserName:(NSString*)userName  andPassword:(NSString*)password withSelector:(SEL) selector;
//-(void)userAuthentication:(NSString *)urlStr parameters:(NSDictionary *)params;
//-(void)loginServices:(NSString *)urlStr parameters:(NSDictionary *)params;

@end

