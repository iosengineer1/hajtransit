//
//  WebServicesURLs.m
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/20/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import "WebServicesURLs.h"

@implementation WebServicesURLs

+(NSString *)getNationalitiesLookupURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,KNationalitiesLookupURL];
    return urlString;
}
+(NSString *)getHotelsLookupURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,KHotelsLookupURL];
    return urlString;
}
+(NSString *)getFlightsURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,kFlightsURL];
    return urlString;
}
+(NSString *)getCitiesLookupURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,KCitiesLookupURL];
    return urlString;
}

+(NSString *)getAppVersionURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,GET_Version];
    return urlString;
}

+(NSString *)getPortsLookupURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,KPortsLookupURL];
    return urlString;
}

+(NSString *)getIstikbalPathLookupURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,KIstikbalPathsLookupURL];
    return urlString;
}

+(NSString *)getSpecialConstractsLookupURL
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,KSpecialConstractsLookupURL];
    return urlString;
}

+(NSString *)getTransportCompaniesLookupURL{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServer,KTransportCompaniesLookupURL];
    return urlString;
}
+(NSString*) getLoginURL{
    
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KLogin_Service_STR];
    return  tempURL ;
}
+(NSString *)getHajMissionURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KHajMissionLookupURL];
    return  tempURL ;
}
+(NSString *)getHajCompaniesURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KHajCompaniesLookupURL];
    return  tempURL ;
}
+(NSString *)getHousingLookupURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KHousingLookupURL];
    return  tempURL ;
}
+(NSString *)getBusDetailsURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KFetchBusDetialsSTR];
    return  tempURL ;
}
+(NSString *)getDriverDetailsURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KFetchDriverDetailsSTR];
    return  tempURL ;
}
+(NSString *)getCreateManifestURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KCreateManifestSTR];
    return  tempURL ;
}
+(NSString *)getManifestURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KManifestListSTR];
    return  tempURL ;
}
+(NSString *)getAssignHajisToManfiestURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KAssignHajisToManifestSTR];
    return  tempURL ;
}
+(NSString *)getRemoveAssignhajiURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KRemoveAssignHajiSTR];
    return  tempURL ;
}
+(NSString *)getCloseManifestURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KCloseManifestSTR];
    return  tempURL ;
}
+(NSString *)getHousesURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KGETHousesSTR];
    return  tempURL ;
}
+(NSString *)getHouseContractsURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KHajHouseContractsSTR];
    return  tempURL ;
}
+(NSString *)getUpdateManifestURL{
    NSString *tempURL=[NSString stringWithFormat:@"%@%@",kWebServer,KUpdateManifestSTR];
    return  tempURL ;
}
@end
