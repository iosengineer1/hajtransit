//
//  InternetConnectionManager.m
//  Umra E-service
//
//  Created by Mohammed Rezk on 11/30/15.
//  Copyright © 2015 sejeltech. All rights reserved.
//

#import "InternetConnectionManager.h"

@implementation InternetConnectionManager



static InternetConnectionManager * sharedInstance = nil;

+(InternetConnectionManager*)instance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        
        [sharedInstance initOthers];
    
    }
    return sharedInstance;
}
-(void) initOthers{
    
//    [AFNetworkReachabilityManager managerForDomain:@"www.google.com"];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    isInternetAvailable=NO;
    
}


-(void) startNetworkMonitiring{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}


-(BOOL) isInternetAvailable{
//    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
//        return YES;
//    }
//    else {
//        [self showAlerView];
//        return NO;
//    }
//
    if (([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)) {
        [self showAlerView];
        return NO;
    }
    else{
         return YES;
    }
}
-(void) showAlerView{

    
    [ISMessages showCardAlertWithTitle:NSLocalizedString(@"No Internet connection ", nil)
                               message:NSLocalizedString(@"Please make sure there is an Internet connection.", nil)
                              duration:4.f
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeError
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
    
}

@end
