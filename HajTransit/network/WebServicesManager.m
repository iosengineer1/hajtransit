//
//  WebService.m
//  DOF
//
//  Created by Mir Taqi Ahmed on 5/5/15.
//  Copyright (c) 2015 TACME. All rights reserved.
//

#import "WebServicesManager.h"

#define kErrorId @"errorId"
#define kErrorDescription @"errorDesc"
#define NO_INTERNET @"No network connection."

@interface WebServicesManager() {
    NSMutableData *receivedData;
    
}
@end
@implementation WebServicesManager

@synthesize completionHandler;
@synthesize errorHandler;



-(BOOL) internetAvailable{
    if (![[InternetConnectionManager instance] isInternetAvailable]) {
        
        return FALSE;
    }
    return TRUE;
}

#pragma mark - Service Calls

- (NSURLSession *)getURLSession
{
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,
                  ^{
                      NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
                      session = [NSURLSession sessionWithConfiguration:configuration];
                  });
    return session;
}

-(void)postRequestFor:(NSString *)urlStr parameters:(NSDictionary *)parameters  withSelector:(SEL) selector{
  
    if (![self internetAvailable]) {
        [[UtilityHandler sharedHandler]hideProgressHUD];
       //No internet message will appear.
        return;
    }
    
    NSError *error;
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
   

    NSString *authValue = @"";
    if ([KeyChain hasValueForKey:K_SAUTH_USER_NAME]&&[KeyChain hasValueForKey:K_SAUTH_PASSWORD]) {
        NSString *authenticationString = [NSString stringWithFormat:@"%@:%@",[KeyChain stringForKey:K_SAUTH_USER_NAME],[KeyChain stringForKey:K_SAUTH_PASSWORD]];
        NSString *base64EncodedString = [[authenticationString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
       authValue = [NSString stringWithFormat:@"Basic %@", base64EncodedString];
        [[manager requestSerializer] setValue:authValue forHTTPHeaderField:@"authorization"];
        
    }
   
    [manager.requestSerializer setTimeoutInterval:120];
    
   
    [manager POST:urlStr parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable response) {
      //  if(![urlStr isEqualToString:[WebServicesURLs getLoginURL]])
        [[UtilityHandler sharedHandler]hideProgressHUD];
        if (response!=nil) {
            NSDictionary * data= (NSDictionary*)response;
            NSDictionary *error = NULL_TO_NIL([data objectForKey:@"error"]);
            if(error){
                int errorId = NULL_TO_NIL([error valueForKey:@"errorId"])!=nil?[[error valueForKey:@"errorId"] intValue]:-1;
              if(errorId == 999999){
                
                    NSString *errorDesc = CHECK_NULL_STRING([error valueForKey:kErrorDescription]);
                 //   [[ErrorHandler instance] logError:@"SERVER_ERROR" errorMSG:errorDesc serviceURL:urlStr serviceJson:jsonString];
                    NSLog(@"Error: %@    <<<<===========999999============>>>", error );
                
                  self.errorHandler(response, selector);
                    
                }
                else {
                     self.completionHandler(response, selector);
                   
                }
            }
            else {
               
                self.completionHandler(response, selector);
            }
        }
        
    }
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        // if(![urlStr isEqualToString:[WebServicesURLs getLoginURL]])
         [[UtilityHandler sharedHandler]hideProgressHUD];
         NSLog(@"Error: %@    =======================>>>>", error.description );

         NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
         NSInteger statusCode = [response statusCode];
         NSString *msg=error.localizedDescription;
         if (statusCode==401 || statusCode==403) {
             msg  = @"بيانات الدخول غير صحيحة او لا يوجد صلاحية "  ;
             self.errorHandler(msg, selector);
         }
         else {
             msg = @" حدث خطأ يرجا المحاولة مرة اخري " ;
            // [self.delegate onRequestFaild:msg  withSelector:selctor];
           //  [[ErrorHandler instance] logError:@"SERVER_ERROR" errorMSG:error.description serviceURL:urlStr serviceJson:jsonString];
             self.errorHandler(response, selector);
         }
     
     }];

}


-(void) postRequest:(NSString*) url  withParamters:(NSMutableDictionary*) parameters  andUserName:(NSString*)userName  andPassword:(NSString*)password withSelector:(SEL) selector{
    
    if (![self internetAvailable]) {
        [[UtilityHandler sharedHandler]hideProgressHUD];
        //No internet message will appear.
        return;
    }
    
    NSError *error;
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@",userName,password];
    NSString *base64EncodedString = [[authenticationString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", base64EncodedString];
    [[manager requestSerializer] setValue:authValue forHTTPHeaderField:@"authorization"];
    
    
    [manager.requestSerializer setTimeoutInterval:120];
    
    
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable response) {
        //  if(![urlStr isEqualToString:[WebServicesURLs getLoginURL]])
        [[UtilityHandler sharedHandler]hideProgressHUD];
        if (response!=nil) {
            NSDictionary * data= (NSDictionary*)response;
            NSDictionary *error = NULL_TO_NIL([data objectForKey:@"error"]);
            if(error){
                int errorId = [[error valueForKey:@"errorId"] intValue];
                if(errorId == 999999){
                    
                    NSLog(@"Error: %@    <<<<===========999999============>>>", error );
                    
                    self.errorHandler(response, selector);
                    
                }
                else {
                    self.completionHandler(response, selector);
                    
                }
            }
            else {
                
                self.completionHandler(response, selector);
            }
        }
        
    }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              // if(![urlStr isEqualToString:[WebServicesURLs getLoginURL]])
              [[UtilityHandler sharedHandler]hideProgressHUD];
              NSLog(@"Error: %@    =======================>>>>", error.description );
              
              NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
              NSInteger statusCode = [response statusCode];
              NSString *msg=error.localizedDescription;
              if (statusCode==401 || statusCode==403) {
                  msg  = @"بيانات الدخول غير صحيحة او لا يوجد صلاحية "  ;
                  self.errorHandler(msg, selector);
              }
              else {
                  msg = @" حدث خطأ يرجا المحاولة مرة اخري " ;
                  // [self.delegate onRequestFaild:msg  withSelector:selctor];
                  //  [[ErrorHandler instance] logError:@"SERVER_ERROR" errorMSG:error.description serviceURL:urlStr serviceJson:jsonString];
                  self.errorHandler(response, selector);
              }
              
          }];
    
}

-(void) fetchData:(NSString*) url  withParamters:(NSMutableDictionary*) parameters withSelector:(SEL) selector{
    if (![self internetAvailable]) {
        //No internet message will appear.
        return;
    }
//    _selector = selector;
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];

    if ([KeyChain hasValueForKey:K_SAUTH_USER_NAME]&&[KeyChain hasValueForKey:K_SAUTH_PASSWORD]) {
        NSString *authenticationString = [NSString stringWithFormat:@"%@:%@",[KeyChain stringForKey:K_SAUTH_USER_NAME],[KeyChain stringForKey:K_SAUTH_PASSWORD]];
        NSString *base64EncodedString = [[authenticationString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
       NSString *authValue = [NSString stringWithFormat:@"Basic %@", base64EncodedString];
        [[manager requestSerializer] setValue:authValue forHTTPHeaderField:@"authorization"];
        
    }
    
       [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable response) {
           
            if (response!=nil) {
                if([response isKindOfClass:[NSDictionary class]]){
                    NSDictionary * data=(NSDictionary *)response;
                    NSDictionary *error = NULL_TO_NIL([data objectForKey:@"error"]);
                    if (error) {
                        int errorId = [[error valueForKey:@"errorId"] intValue];
                        if (errorId == 0) {
                            NSArray * data=(NSArray *)response;
                            self.completionHandler(data,selector);
                        }
                        else if(errorId == 999999){
                            
                            
                          //  NSString *errorDesc = CHECK_NULL_STRING([error valueForKey:kErrorDescription]);
                         //   [[ErrorHandler instance] logError:@"SERVER_ERROR" errorMSG:errorDesc serviceURL:url serviceJson:@""];
                            
                        }
                        else {
                            NSString *errorDesc = CHECK_NULL_STRING([error valueForKey:@"errorDesc"]);
                            self.errorHandler(errorDesc,selector);
                        }
                    }
                    else {
                       self.errorHandler(nil,selector);
                    
                    }
                    
                    
                }
                //for lookups response is coming as whole Array
                else{
                    NSArray * data=(NSArray *)response;
                    self.completionHandler(data,selector);
                }
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            
                NSString *msg=task.description;
                //[self.delegate onRequestFaild:msg  withSelector:selctor];
        
            //    [[ErrorHandler instance] logError:@"SERVER_ERROR" errorMSG:error.description serviceURL:url serviceJson:@""];
                self.errorHandler(msg,selector);
                
            
        }];
        
}
    

@end

