//
//  WebServicesURLs.h
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/20/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import <Foundation/Foundation.h>

// Domain URL
//#define kWebServer @"https://sox.sejeltech.com/eHajMobileServices-DEV/" //Dev
#define kWebServer @"https://sox.sejeltech.com/eHajMobileServices-QA/" //Qa
//#define kWebServer @"https://eservicesm.haj.gov.sa/eHajMobileServices/" //Production


#define KSERVICE_ERROR @"app/serviceError"
#define KLogin_Service_STR @"IstkbalUser/loginUser"


//lookups
#define KNationalitiesLookupURL @"Istkballookup/nationalities"
#define KHotelsLookupURL @"Istkballookup/getHotels"
#define KCitiesLookupURL @"Istkballookup/getCities"
#define kFlightsURL @"Istkballookup/getFlights"

#define KPortsLookupURL @"Istkballookup/getPorts"
#define KIstikbalPathsLookupURL @"Istkballookup/getIstikbalPaths"
#define KSpecialConstractsLookupURL @"Istkballookup/getSpecialConstracts"
#define KTransportCompaniesLookupURL @"Istkballookup/landTrans"
#define KHousingLookupURL @"Istkballookup/houses"
#define KGETHousesSTR @"IstkbalRS/getHouses"
#define KHajMissionLookupURL @"Istkballookup/hajjMissions"
#define KHajCompaniesLookupURL @"Istkballookup/hajjCompanies"
#define KHajHouseContractsSTR @"IstkbalRS/getHouseContracts"


//Bus Details
#define KFetchBusDetialsSTR @"IstkbalRS/searchBussDetails"
//Driver Details
#define KFetchDriverDetailsSTR @"IstkbalRS/searchDriverDetailsByID"
//Create Manifest
#define KCreateManifestSTR @"IstkbalRS/CreateManifist"
//Update Manifest
#define KUpdateManifestSTR @"IstkbalRS/UpdateManifist"
//List of Manifests
#define KManifestListSTR @"IstkbalRS/getManifistList"
//
#define KAssignHajisToManifestSTR @"IstkbalRS/assignHajjsToManifist"
//
#define KRemoveAssignHajiSTR @"IstkbalRS/removeAssignedHajj"
//
#define KCloseManifestSTR @"IstkbalRS/CloseManifist"


#define GET_Version @"/app/checkVersion"

@interface WebServicesURLs : NSObject
+(NSString *)getHotelsLookupURL;
+(NSString *)getNationalitiesLookupURL;
+(NSString *)getCitiesLookupURL;
+(NSString *)getFlightsURL;
+(NSString *)getAppVersionURL;

+(NSString *)getPortsLookupURL;
+(NSString *)getIstikbalPathLookupURL;
+(NSString *)getSpecialConstractsLookupURL;
+(NSString *)getTransportCompaniesLookupURL;
+(NSString *)getHajMissionURL;
+(NSString *)getHajCompaniesURL;
+(NSString *)getHousingLookupURL;
+(NSString *)getAssignHajisToManfiestURL;
+(NSString *)getRemoveAssignhajiURL;
+(NSString *)getCloseManifestURL;
+(NSString *)getUpdateManifestURL;

+(NSString *)getHousesURL;
+(NSString *)getHouseContractsURL;

+(NSString *)getBusDetailsURL;
+(NSString *)getDriverDetailsURL;
+(NSString *)getCreateManifestURL;
+(NSString *)getManifestURL;
+(NSString*) getLoginURL;







@end
