//
//  InternetConnectionManager.h
//  Umra E-service
//
//  Created by Mohammed Rezk on 11/30/15.
//  Copyright © 2015 sejeltech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface InternetConnectionManager : NSObject
{
     BOOL    isInternetAvailable;
    UIAlertController * alert;
}
+ (InternetConnectionManager * ) instance ;
-(void) startNetworkMonitiring;
-(BOOL) isInternetAvailable;
-(BOOL)isInternetAvailable;
@end
