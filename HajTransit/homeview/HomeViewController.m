//
//  HomeViewController.m
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/23/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import "HomeViewController.h"

#define  Image_TAG 1
#define Title_TAG 2
@interface HomeViewController ()
{
    
    IBOutlet UIBarButtonItem *sidebarButton;
    __weak IBOutlet UITableView *tblView;
    NSArray *data;
}
-(IBAction)gotoSettings:(id)sender;

@end

@implementation HomeViewController

-(IBAction)gotoSettings:(id)sender{
    [self performSegueWithIdentifier:NSStringFromClass([SideViewController class]) sender:nil];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
   // manifestListController = [Main_Story instantiateViewControllerWithIdentifier:NSStringFromClass([ManifestListViewController class])];
    NSLog(@"App Delegate = %@", [appDelegate.persistentContainer viewContext] );
    data = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Home" ofType:@"plist"]];
    [tblView reloadData];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.title = SejelLocalizedString(@"Istikbal", nil);
    [[appDelegate mainNavigationViewController] setNavigationBarHidden:NO animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
//    if([segue.identifier isEqualToString:NSStringFromClass([HistoryViewController class])]){
//
//    }
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80.0f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:Image_TAG];
    UILabel *label = (UILabel *)[cell viewWithTag:Title_TAG];
    NSDictionary *record = data[indexPath.row];
    imageView.image = [UIImage imageNamed:[record valueForKey:@"image"]];
    label.text = [record valueForKey:[def boolForKey:IS_LANGUAGE_ENGLISH]?@"title":@"title_ar"];
    UIImageView* arrowImage = (UIImageView*)[cell viewWithTag:47];
    if (![def boolForKey:IS_LANGUAGE_ENGLISH]) {
        [arrowImage setImage:[UIImage imageNamed:@"Left Arrow"]];
    }
    // Configure the cell...
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *record = data[indexPath.row];
    NSString *identifier = [record objectForKey:@"identifier"];
//    if([identifier isEqualToString:NSStringFromClass([ManifestListViewController class])]){
//        if(![[self.navigationController viewControllers]containsObject:manifestListController])
//        [self.navigationController pushViewController:manifestListController animated:YES];
//    }
//    else
//    [self performSegueWithIdentifier:[record objectForKey:@"identifier"] sender:nil];
//
    //  BOOL status = [[EPOSPrinter sharedInstance]runPrintReceiptSequence];
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

@end

