//
//  LoginViewController.m
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/22/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
{
    
    UITextField *txtField;
    WebServicesManager * webServicesManager;
    NSArray *dropDownData;
    
    __weak IBOutlet UITableView *tblView;

    UITextField *textFieldUserName, *textFieldPassword;
    UIToolbar *toolBar;
    NSArray *data;

}
@end

@implementation LoginViewController

-(void)viewDidAppear:(BOOL)animated{
    [[appDelegate mainNavigationViewController] setNavigationBarHidden:YES];

}
-(void)doneEditing:(id)sender{
    [txtField resignFirstResponder];
}
-(void)createToolBar{
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditing:)];
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    toolBar.items = [def boolForKey:IS_LANGUAGE_ENGLISH]?[NSArray arrayWithObjects:flexibleWidth,flexibleWidth,doneBtn,nil]:[NSArray arrayWithObjects:doneBtn,flexibleWidth,flexibleWidth,nil];
    toolBar.barStyle = UIBarStyleDefault;
    
}
-(void)setLocalizations{
    UILabel *loginLabel = [self.view viewWithTag:100];
    UILabel *loginFooterLabel = [self.view viewWithTag:200];
    
    if(loginLabel!=nil)
        loginLabel.text = SejelLocalizedString(@"Login", nil);
    if(loginFooterLabel!=nil)
        loginFooterLabel.text = SejelLocalizedString(@"Login", nil);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    webServicesManager = [[WebServicesManager alloc] init];
    [self setWebServicesBlocksHAndlers];
    [self createToolBar];
    data = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Login" ofType:@"plist"]];
    [self setLocalizations];
    [tblView reloadData];
}


-(void) setWebServicesBlocksHAndlers {
    __block LoginViewController *blocksafeSelf = self;
    [webServicesManager setCompletionHandler:^(id dict, SEL selector) {
        [MBProgressHUD hideHUDForView:blocksafeSelf.view animated:true];
        if (selector == @selector(loginServiceResponse:)) {
            [blocksafeSelf loginServiceResponse:dict];
        }
    }];
    
    [webServicesManager setErrorHandler:^(id resp, SEL selector) {
        [MBProgressHUD hideHUDForView:blocksafeSelf.view animated:true];
      
        if([resp isKindOfClass:[NSString class]])
        [[UtilityHandler sharedHandler]showAlertForController:self withMessage:[resp description]];

    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
   
}


-(void)updateFieldsStatusIfEmpty{
    NSInteger row = 0;
    for(NSDictionary *record in data){
        InputTextTableCell *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
        if([cell respondsToSelector:@selector(textField)])
        {
            if(cell.textField.text.length==0){
                cell.cellValidationStatusLabel.text = NSLocalizedString(@"Required", nil);
            }
        }
        row++;
    }
}
-(BOOL)validationPassed{
    
    if(textFieldPassword.text.length==0 ||textFieldUserName.text.length==0){
        [self updateFieldsStatusIfEmpty];
        [[UtilityHandler sharedHandler]showAlertForController:self withMessage:NSLocalizedString(@"All fields are required.",nil ) withTitle: NSLocalizedString(@"ValidationTitleAlert", nil)];
        return false;
    }
    NSInteger row = 0;
    for(NSDictionary *record in data){
        NSString *identifier = [record objectForKey:@"identifier"];
        if([identifier isEqualToString:@"CELL_TEXTFIELD_IDENTIFIER"]){
            InputTextTableCell *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
            if(cell.cellValidationStatusLabel.text.length>0)
            {
                [[UtilityHandler sharedHandler]showAlertForController:self withMessage:NSLocalizedString(@"Please enter valid data",nil ) withTitle: NSLocalizedString(@"ValidationTitleAlert", nil)];
                return false;
            }
        }
        row++;
    }
    return true;
}
- (IBAction)performLogin:(id)sender {
    [txtField resignFirstResponder];
    if ([self validationPassed]&&[[InternetConnectionManager instance] isInternetAvailable]) {
        [self performLoginService];
    }
}

-(void) performLoginService {
   
    
  MBProgressHUD* hud =   [MBProgressHUD showHUDAddedTo:self.view animated:true];
   
    hud.animationType = MBProgressHUDAnimationZoom;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.backgroundView.color =  [UIColor colorWithWhite:0.f alpha:.4f];
    
    NSMutableDictionary * paramters = [NSMutableDictionary dictionary];
    [paramters setValue:textFieldUserName.text forKey:@"userName"];
    [paramters setValue:textFieldPassword.text forKey:@"password"];
   
    [webServicesManager postRequest:[WebServicesURLs getLoginURL] withParamters:paramters andUserName:textFieldUserName.text andPassword:textFieldPassword.text withSelector:@selector(loginServiceResponse:)];
    
}
-(void) loginServiceResponse:(id) resp {
    
    if (resp != nil) {
        NSDictionary * responseData = (NSDictionary*)resp;
        NSDictionary * data_error = [responseData objectForKey:@"error"];
        if (data_error != nil) {
            int errorId = [[data_error valueForKey:@"errorId"] intValue];
            NSString * errorDesc = [data_error valueForKey:@"errorDesc"];
            if (errorId == 0 ) {
                KeyChain.useAccessControl = YES;
                [KeyChain setString:textFieldUserName.text forKey:K_SAUTH_USER_NAME];
                [KeyChain setString:textFieldPassword.text forKey:K_SAUTH_PASSWORD];
                 [KeyChain setString:[NSString stringWithFormat:@"%@",[responseData objectForKey:@"userId"]] forKey:K_USER_ID];
                if(NULL_TO_NIL([responseData objectForKey:K_USER_PORTS])!=nil)
                {
                    [def setObject:[responseData objectForKey:K_USER_PORTS] forKey:K_USER_PORTS];
                }
                NSArray *data = [DBManager fetchRequestForEntity:ENTITY_PORTS predicate:nil fetchLimit:NO sortDescriptor:nil dictionaryType:NO];
                if(data!=nil){
                    if(data.count==0)
                        [[LookupsManager sharedLookupsHandler]startSyncing];
                }
                [[UtilityHandler sharedHandler]setUserLoggedIn];
                if(NULL_TO_NIL([responseData objectForKey:IS_USER_ADMIN])!=nil)
                [[UtilityHandler sharedHandler]setUserRoleWithStatus:[[responseData objectForKey:IS_USER_ADMIN]boolValue]];
                [appDelegate gotoHomeView];
            
            }
            else {
               
                [[UtilityHandler sharedHandler]showAlertForController:self withMessage:errorDesc withTitle:NSLocalizedString(@"ValidationTitleAlert", nil)];
                
            }
        }
    }
}


#pragma mark- TableViewDataSource

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return data.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = [data[indexPath.row]valueForKey:@"identifier"];
    // if([identifier isEqualToString:@"CELL_DROPDOWN_IDENTIFIER"]){
    InputTextTableCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *record = data[indexPath.row];
    cell.delegate = self;
    if(![def boolForKey:IS_LANGUAGE_ENGLISH])
        cell.textField.textAlignment = NSTextAlignmentRight;
    if(![def boolForKey:IS_LANGUAGE_ENGLISH]&&[cell isKindOfClass:[DropDownTableCell class]])
    [cell.cellDirectionImageView setTransform:CGAffineTransformMakeScale(-1, 1)];
    cell.textField.placeholder =  [def boolForKey:IS_LANGUAGE_ENGLISH]?[data[indexPath.row]valueForKey:@"placeholder_en"]:[data[indexPath.row]valueForKey:@"placeholder_ar"];
    cell.cellHeaderLabel.text =  [def boolForKey:IS_LANGUAGE_ENGLISH]?[data[indexPath.row]valueForKey:@"cellHeaderTitle_en"]:[data[indexPath.row]valueForKey:@"cellHeaderTitle_ar"];
    cell.iconImageView.image = [UIImage imageNamed:[data[indexPath.row]valueForKey:@"cell_icon"]];
    [cell.textField addTarget:cell action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged ];
    if([record objectForKey:@"textFieldType"]!=nil){
        if([[record objectForKey:@"textFieldType"]isEqualToString:@"password"])
            cell.textField.secureTextEntry = YES;
        else
            cell.textField.secureTextEntry = NO;
    
    }
    cell.textField.textContentType = @"";
    cell.fieldName = [record objectForKey:@"fieldName"];
    if([cell respondsToSelector:@selector(textField)]){
    if(cell.textField.text.length==0)
        cell.cellValidationStatusLabel.text = @"";}
    

    return cell;
   
   
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DropDownTableCell *cell = (DropDownTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:[cell.textField respondsToSelector:@selector(isFirstResponder)]?NO:YES];

    txtField = cell.textField;
    if([cell.textField respondsToSelector:@selector(isFirstResponder)]){
        [cell.textField becomeFirstResponder];
    }
   
}

#pragma mark- CellDelegate
-(void)textFieldDidFinishEditing:(UITextField *)textField forCell:(InputTextTableCell *)cell{
   
    if([cell.fieldName isEqualToString:USER_NAME_FIELD])
        textFieldUserName = textField;
    else if([cell.fieldName isEqualToString:PASSWORD_FIELD])
        textFieldPassword = textField;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
  // textField.inputAccessoryView = toolBar;
    txtField = textField;
  
}

@end
