//
//  LoginViewController.h
//  MuatamarTransact
//
//  Created by Mohammed Mir on 11/22/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilityHandler.h"
#import "InputTextTableCell.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate,InputTextTableCellDelegate, UITableViewDelegate>

- (IBAction)performLogin:(id)sender;




@end
