//
//  AppDelegate.h
//  HajTransit
//
//  Created by Mohammed Mir on 05/06/2018.
//  Copyright © 2018 Mohammed Mir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MainNavigationViewController.h"
#import "LoginViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic , strong) MainNavigationViewController *mainNavigationViewController;
- (void)saveContext;
-(void)relaunchApp;
-(void)gotoLoginView;
-(void)gotoHomeView;


@end

