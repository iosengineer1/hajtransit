//
//  LocalizationSystem.h
//  Istikbal
//
//  Created by Mohammed Mir on 22/05/2018.
//  Copyright © 2018 Sejel Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SejelLocalizedString(key, comment) \
[[LocalizationSystem sharedLocalSystem] localizedStringForKey:(key) value:(comment)]

#define LocalizationSetLanguage(language) \
[[LocalizationSystem sharedLocalSystem] setLanguage:(language)]

#define LocalizationGetLanguage \
[[LocalizationSystem sharedLocalSystem] getLanguage]

#define LocalizationReset \
[[LocalizationSystem sharedLocalSystem] resetLocalization]


@interface LocalizationSystem : NSObject
{
    NSString *language;
}
@property(nonatomic)BOOL localizationSelected;
+ (LocalizationSystem *)sharedLocalSystem;

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;
- (void) setLanguage:(NSString*) language;

- (NSString*) getLanguage;

- (void) resetLocalization;
@end
