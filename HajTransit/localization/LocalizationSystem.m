//
//  LocalizationSystem.m
//  Istikbal
//
//  Created by Mohammed Mir on 22/05/2018.
//  Copyright © 2018 Sejel Technology. All rights reserved.
//

#import "LocalizationSystem.h"

@implementation LocalizationSystem

//Singleton instance
static LocalizationSystem *_sharedLocalSystem = nil;

//Current application bungle to get the languages.
static NSBundle *bundle = nil;

+ (LocalizationSystem *)sharedLocalSystem
{
    @synchronized([LocalizationSystem class])
    {
        if (!_sharedLocalSystem){
          _sharedLocalSystem =  [[self alloc] init];
        }
        return _sharedLocalSystem;
    }
    // to avoid compiler warning
    return nil;
}

+(id)alloc
{
    @synchronized([LocalizationSystem class])
    {
        NSAssert(_sharedLocalSystem == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedLocalSystem = [super alloc];
        return _sharedLocalSystem;
    }
    // to avoid compiler warning
    return nil;
}


- (id)init
{
    if ((self = [super init]))
    {
        //empty.
        bundle = [NSBundle mainBundle];
    }
    return self;
}
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment
{
    return [bundle localizedStringForKey:key value:comment table:nil];
}


- (void) setLanguage:(NSString*) l{
    
    NSString *path = [[ NSBundle mainBundle ] pathForResource:l ofType:@"lproj" ];
    
    if (path == nil)
        //in case the language does not exists
        [self resetLocalization];
    else
        bundle = [NSBundle bundleWithPath:path];
}

- (NSString*) getLanguage{
    
    NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    
    NSString *preferredLang = [languages objectAtIndex:0];
    
    return preferredLang;
}

- (void) resetLocalization
{
    bundle = [NSBundle mainBundle];
}

@end
