//
//  MainNavigationViewController.m
//  MuatamarTransact
//
//  Created by Mohammed  Rezk on 12/3/17.
//  Copyright © 2017 Mohammed Mir. All rights reserved.
//

#import "MainNavigationViewController.h"

@interface MainNavigationViewController ()

@end

@implementation MainNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [appDelegate setMainNavigationViewController:self];
   
    if([[UtilityHandler sharedHandler]isUserLoggedIn]){
        [appDelegate gotoHomeView];
    }
    else{
        [appDelegate gotoLoginView];
    }
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
